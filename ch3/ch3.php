<?php

/**
 * @author jlaso@joseluislaso.es
 *
 * https://contest.tuenti.net/resources/2012/Question_3.html
 */

/**
 * Class StockDecision
 *
 * This class accumulates stock options and decides in the BACKDOOR time what is the
 * best stock option (time) to buy and which to sell.
 *
 *
 */
class StockDecision
{

    const DEBUG_ON   = true;
    const DEBUG_OFF  = false;

    const RESOLUTION = 100;
    const BACKDOOR   = 1000;

    protected $values;
    protected $remainingTime;

    protected $buyAt;
    protected $sellAt;
    protected $benefits;

    protected $debug;

    /**
     * Constructor
     *
     * @param bool $debug
     */
    public function __construct($debug = self::DEBUG_OFF)
    {
        $this->values        = array();
        $this->remainingTime = self::BACKDOOR;
        $this->debug         = $debug;
    }

    /**
     * Prints string if debug is active
     *
     * @param $str
     */
    public function debug($str)
    {
        if ($this->debug) {
            print $str;
        }
    }

    /**
     * Dumps if debug is active
     *
     * @param $mixed
     */
    public function dump($mixed)
    {
        if ($this->debug) {
            var_dump($mixed);
        }
    }

    /**
     * computes a value and if not remaining time calls decide
     *
     * @param $value
     *
     * @return bool   false if remaining time, true otherwise
     */
    public function computeValue($value)
    {
        $this->values[] = intval($value);
        $this->debug(sprintf("%s", $value));
        $this->remainingTime -= self::RESOLUTION;
        if ($this->remainingTime < 0) {
            $this->decide();

            return true;
        }

        return false;
    }

    /**
     * Decides into the values stored which are the better moment to buy and the better to sell
     * and stores this info into this object
     *
     */
    public function decide()
    {
        $values          = $this->values;
        $maxBenefit      = 0;
        $maxIndexBenefit = null;
        $minIndexBenefit = null;
        $index           = 1;

        while (count($values) > 1)
        {
            $this->debug(sprintf('* - * - * - * (( %d  )) * - * - * - *', $index) . PHP_EOL);
            $this->debug(json_encode($values) . PHP_EOL);
            // gets the max of values
            $max = max($values);
            $this->debug(sprintf('%d: max(%d)', $index, $max) . PHP_EOL);
            // and calc her index
            $maxIndex = array_search($max, $values);
            //$this->debug('maxindex ' . $maxIndex . PHP_EOL);
            // if there is max value (and isn't in first position)
            if ($maxIndex > 0) {
                // the min value (buy better moment) must to be before the max value (better sell moment)
                $subValues = array_slice($values, 0, $maxIndex, true);
                $this->debug(json_encode($subValues) . PHP_EOL);
                // now, gets min value of this subset
                $min       = min($subValues);
                $this->debug(sprintf('%d: max(%d) min(%d)', $index, $max, $min) . PHP_EOL);
                // and her index
                $minIndex = array_search($min, $subValues);
                $this->debug('minindex ' . $minIndex . PHP_EOL);
                // calc the benefit for this action (better buy and better sell)
                $benefit  = $max - $min;
                // if is better benefit that stored to now
                if ($benefit > $maxBenefit) {
                    $maxBenefit = $benefit;
                    $maxIndexBenefit = $maxIndex;
                    $minIndexBenefit = $minIndex;
                    $this->debug(sprintf('***>>> (%d,%d)', $maxIndexBenefit, $minIndexBenefit) . PHP_EOL);
                }
            }
            // erases the max value processed
            unset($values[$maxIndex]);
            $index ++;
        }

        // store into this object info about better buy, better sell and his benefit
        // the info of moments are stored in milliseconds resolution
        $this->setBenefits($maxBenefit);
        $this->setBuyAt($minIndexBenefit * self::RESOLUTION);
        $this->setSellAt($maxIndexBenefit * self::RESOLUTION);

        $this->debug(sprintf('max benefit %d buying at %d and selling at %d',
            $maxBenefit, $this->getBuyAt(), $this->getSellAt()) . PHP_EOL);
    }

    // setter for benefits
    public function setBenefits($benefits)
    {
        $this->benefits = $benefits;
    }

    // getter for benefits
    public function getBenefits()
    {
        return $this->benefits;
    }

    // setter for better buy moment
    public function setBuyAt($buyAt)
    {
        $this->buyAt = $buyAt;
    }

    // getter for better buy moment
    public function getBuyAt()
    {
        return $this->buyAt;
    }

    // setter for better sell moment
    public function setSellAt($sellAt)
    {
        $this->sellAt = $sellAt;
    }

    // getter for better sell moment
    public function getSellAt()
    {
        return $this->sellAt;
    }



}


// start of program
$stockDec  = new StockDecision(StockDecision::DEBUG_OFF);
$f         = fopen('php://stdin', 'r');
while ($value = fgets($f))
{
    // read values in one second (StockDecision::BACKDOOR)
    if ($stockDec->computeValue($value))
    {
        break;
    }
}
fclose($f);

print $stockDec->getBuyAt() . ' ';
print $stockDec->getSellAt() . ' ';
print $stockDec->getBenefits() . PHP_EOL;