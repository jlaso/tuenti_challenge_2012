<?php

/**
 * @author Joseluis Laso <jlaso@joseluislaso.es>
 *
 * Tuenti Challenge 2012, exercise 2
 * https://contest.tuenti.net/resources/2012/Question_2.html
 */


function maxNumOfOnesSumatory($value)
{
    $allOnesPrevCnt     = strlen(decbin($value)) - 1;
    $allOnesPrev        = str_repeat('1', $allOnesPrevCnt);
    $allOnesPrevDecimal = bindec($allOnesPrev);
    $difference         = decbin($value - $allOnesPrevDecimal);
    $concatenated       = $difference . $allOnesPrev;

    return substr_count($concatenated, '1');
}

$values    = array();
$f         = fopen('php://stdin', 'r');
$numValues = intval(fgets($f));
for ($i = 0; $i < $numValues; $i ++) {
    $values[] = intval(fgets($f));
}
fclose($f);
$i = 1;
foreach ($values as $value) {
    print sprintf("Case #%d: %d", $i, maxNumOfOnesSumatory($value)) . PHP_EOL;
    $i ++;
}