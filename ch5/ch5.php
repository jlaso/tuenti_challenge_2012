<?php


/**
 * @author Joseluis Laso <jlaso@joseluislaso.es>
 *
 * https://contest.tuenti.net/resources/2012/Question_5.html
 */

interface ClockInterface
{


    //public function setTime($time);
    public function getDeltaTo($time);


}

abstract class DigitalClock
{
    // number of leds that have "ON" each digit
    const DIGIT_0 = 6;
    const DIGIT_1 = 2;
    const DIGIT_2 = 5;
    const DIGIT_3 = 5;
    const DIGIT_4 = 4;
    const DIGIT_5 = 5;
    const DIGIT_6 = 6;
    const DIGIT_7 = 3;
    const DIGIT_8 = 7;
    const DIGIT_9 = 6;

    private $digits;
    private $matrix;

    private $currentTime;

    public function __construct()
    {
        $this->digits = array(
            self::DIGIT_0,
            self::DIGIT_1,
            self::DIGIT_2,
            self::DIGIT_3,
            self::DIGIT_4,
            self::DIGIT_5,
            self::DIGIT_6,
            self::DIGIT_7,
            self::DIGIT_8,
            self::DIGIT_9
        );

        $this->matrix = $this->getMatrix();
    }

    public function getMatrix()
    {
        $result = array();
        for($index=0; $index<10; $index++)
        {
            $result[$index] = $this->digits;
        }
        return $result;
    }

    public function setCurrentTime($time)
    {
        $this->currentTime = $time;
    }

    /**
     * @param int $digit
     *
     * @return int
     */
    public function ledsON_digit(/*int*/ $digit)
    {
        return $this->digits[$digit];
    }

    /**
     * @param string $string
     *
     * @return int
     */
    public function ledsON_string(/*string*/ $string)
    {
        $result = 0;
        $str = str_split($string);
        foreach ($str as $char) {
            $result += $this->ledsON_digit(intval($char));
        }

        return $result;
    }

    public function deltaDigit(/*int*/ $digitStart, /*int*/ $digitEnd)
    {
        return $this->matrix[$digitStart][$digitEnd];
    }

    public function delta(/*string*/ $stringStart, /*string*/ $stringEnd)
    {
        $result = 0;
        $strStart = str_split($stringStart);
        $strEnd   = str_split($stringEnd);
        foreach ($strStart as $index=>$charStart) {
            $charEnd = $strEnd[$index];
            $result += $this->deltaDigit(intval($charStart),intval($charEnd));
        }

        return $result;
    }

    /**
     * Delta for 60 min or 60 secs
     *
     * @return int
     */
    public function delta60()
    {
        $result = 0;
        for ($digits = 0; $digits<60; $digits++) {
            //$result += $this->ledsON_string('00');
            $result += $this->ledsON_string('' . ($digits > 0 ? $digits : '0' . $digits));
        }

        return $result;
    }

    /**
     * Delta for 24
     *
     * @return int
     */
    public function delta24()
    {
        $result = 0;
        for ($digits = 0; $digits<24; $digits++) {
            $result += $this->ledsON_string('' . ($digits > 0 ? $digits : '0' . $digits));
        }

        return $result;
    }

    /**
     * @param $date
     *
     * @return int
     */
    public function ledsON_time(/*date*/ $date)
    {
        return $this->ledsON_string(date('His', $date));
    }

    public function setTime($time)
    {
        $this->time = $time;
    }

    public function getDeltaTo($time)
    {
        $acum = 0;
        $currentTime = $this->time;
        while ($currentTime<=$time){
            $acum += $this->ledsON_time($currentTime);
            $currentTime = 1; // + date("U", $currentTime);
            print "." . $acum;
        }

        return $acum;
    }

    public function getDelta($startDate, $endDate)
    {

        $delta    = 0;
        $start    = date("U", $startDate);
        $end      = date("U", $endDate);
        $previous = $start;
        $trans    = $this->getMatrix();
//        print sprintf("%s to %s".PHP_EOL, $start, $end);

        for ($current = $start; $current <= $end; $current++)
        {

            if ($previous==$current) {
                continue;
            }
            $prev = date("His", $previous);
            $curr = date("His", $current);
//            print sprintf("%s : %s".PHP_EOL, $prev, $curr);

            for ($digit=0; $digit<6; $digit++)
            {
                $delta += $trans[substr($prev,$digit,1)][substr($curr,$digit,1)];
            }
            $previous = $current;
        }

        return $delta;
    }

}

class OldClock extends DigitalClock
{


}

class NewClock extends DigitalClock
{

    public function getMatrix()
    {
        return array( // 0  1  2  3  4  5  6  7  8  9
            '0' => array(0, 0, 1, 1, 1, 1, 1, 0, 1, 1),
            '1' => array(4, 0, 4, 3, 2, 3, 5, 1, 5, 4),
            '2' => array(2, 1, 0, 1, 2, 2, 1, 1, 2, 2),
            '3' => array(2, 0, 1, 0, 1, 1, 2, 0, 2, 1),
            '4' => array(3, 0, 2, 2, 0, 2, 3, 0, 3, 2),
            '5' => array(2, 1, 2, 1, 1, 0, 1, 1, 2, 1),
            '6' => array(1, 1, 1, 1, 1, 0, 0, 1, 1, 1),
            '7' => array(3, 0, 3, 2, 2, 3, 4, 0, 4, 3),
            '8' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
            '9' => array(1, 0, 1, 0, 0, 0, 1, 0, 1, 0),
        );
    }

}


// start of program
//$f = fopen('php://stdin', 'r');
$oldClock = new OldClock();
$newClock = new NewClock();
while (1/*$input = fgets($f)*/)
{
//    list($dateStart, $dateEnd) = explode(" - ", $input);
//    print sprintf("%s - %s", $dateStart, $dateEnd) . PHP_EOL;
//    $dateStart = strtotime(trim($dateStart));
//    $dateEnd   = strtotime(trim($dateEnd));
//    print sprintf("%s - %s => %d", $dateStart, $dateEnd, $dateEnd - $dateStart) . PHP_EOL;
//
//    // calcs one day
//    $oldClock->setTime(strtotime('2013-01-01 00:00:00'));
//    //print $oldClock->getDeltaTo(strtotime('2013-01-01 23:59:59')) . PHP_EOL;
//
//    print '1 day => ' . $oldClock->delta24() * $oldClock->delta60() * $oldClock->delta60() . PHP_EOL;

    //print 'old ->' . $oldClock->deltaDigit(0,1) . PHP_EOL;
//    print 'old ->' . $oldClock->delta('000000','000001') . PHP_EOL;
    $date1 = new \DateTime();
    $date1->setTime(0,0,0);
    $date2 = clone $date1;
    $date2->modify('+1 day');
    $date2->setTime(23,59,59);
    print 'old\' ->' . $oldClock->getDelta($date1->getTimestamp(),$date2->getTimestamp()) . PHP_EOL;
break;
    //print 'new ->' . $newClock->deltaDigit(0,1) . PHP_EOL;
    print 'new ->' . $newClock->delta('000059','000060') . PHP_EOL;

    $oldClock->setTime($dateStart);
    $newClock->setTime($dateStart);
    //print $oldClock->getDeltaTo($dateEnd) . PHP_EOL;
    //print $newClock->getDeltaTo($dateEnd) . PHP_EOL;
}
//fclose($f);
