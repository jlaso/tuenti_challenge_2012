<?php

/**
 * @author jlaso@joseluislaso.es
 *
 * https://contest.tuenti.net/resources/2012/Question_4.html
 */

/**
 * Class Circuit
 */
class Circuit
{
    const DEBUG_ON   = true;
    const DEBUG_OFF  = false;
    protected $debug;

    protected $karts;
    protected $groups;
    protected $races;
    protected $groupComposition;

    protected $litres;

    /**
     * Constructor
     *
     * @param bool $debug
     */
    public function __construct($options)
    {
        $options      = array_merge(
            array(
                'debug' => self::DEBUG_OFF,
            ),
            $options
        );
        $this->karts  = $options['karts'];
        $this->groups = $options['groups'];
        $this->races  = $options['races'];
        $this->litres = 0;
        $this->debug  = $options['debug'];
        $this->dump($options);

    }

    /**
     * Prints string if debug is active
     *
     * @param $str
     */
    public function debug($str)
    {
        if ($this->debug) {
            print $str;
        }
    }

    /**
     * Dumps if debug is active
     *
     * @param $mixed
     */
    public function dump($mixed)
    {
        if ($this->debug) {
            var_dump($mixed);
        }
    }

    /**
     * @param $groupComposition
     */
    public function setGroupComposition($groupComposition)
    {
        $this->dump($groupComposition);
        $this->groupComposition = $groupComposition;
    }

    /**
     * @return mixed
     */
    public function getGroupComposition()
    {
        return $this->groupComposition;
    }

    /**
     * simulates execute on turn and rotate groups and updates litres consumption
     *
     * @return bool
     */
    public function goRace()
    {
        if ($this->races <= 0) {
            return false;
        }
        $rest             = $this->karts;
        $kartsOccupied    = 0;
        $groupComposition = $this->groupComposition;
        $auxComposition   = array();
        while (count($groupComposition))
        {
            $group = array_shift($groupComposition);
            $this->debug(sprintf('Group %d', $group) . PHP_EOL);
            if ($group <= $rest) {
                $rest -= $group;
                $kartsOccupied += $group;
                $auxComposition[] = $group;
                //unset($groupComposition[0]);
            }else{
                array_unshift($groupComposition, $group);
                break;
            }
            //if (!count($groupComposition)) break;
        }
        foreach ($auxComposition as $value) {
            $groupComposition[] = $value;
        }
        $this->groupComposition = $groupComposition;
        // update litres consumption
        $this->litres += $kartsOccupied;
        $this->races--;
        return true;
    }

    /**
     * get the litres that karts has been consumed in races
     *
     * @return int
     */
    public function getLitres()
    {
        return $this->litres;
    }


}


// start of program
$f         = fopen('php://stdin', 'r');
$testCases = intval(fgets($f));
//print sprintf('%d test cases', $testCases) . PHP_EOL;
$index     = 1;
while ($index <= $testCases) {
    $rkg    = fgets($f);
    list($races,$karts,$groups) = explode(" ", $rkg);
    $gComp  = fgets($f);
    $groupComposition = explode(' ', $gComp);
    $circuit = new Circuit(array(
        //'debug'  => Circuit::DEBUG_ON,
        'races'  => $races,
        'karts'  => $karts,
        'groups' => $groups,
    ));
    $circuit->setGroupComposition($groupComposition);
    while ($circuit->goRace()) {}
    print $circuit->getLitres() . PHP_EOL;
    $index++;
}
fclose($f);
