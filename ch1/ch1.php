<?php
/**
 * @author Joseluis Laso <jlaso@joseluislaso.es>
 * Date: 10/04/13
 * Time: 22:26
 * https://contest.tuenti.net/resources/2012/Question_1.html
 */

class Button
{
    const UP = -1;
    protected $keys;
    protected $col, $row;

    public function __construct($data = array())
    {
        $this->keys = $data['keys'];
        $this->col  = $data['col'];
        $this->row  = $data['row'];
    }

    public function hasKey($key)
    {
        if (is_array($this->keys)) {
            return array_search($key, $this->keys);
        }else{
            return false;
        }
    }

    public function setCol($col)
    {
        $this->col = $col;
    }

    public function getCol()
    {
        return $this->col;
    }

    public function setKeys($keys)
    {
        $this->keys = $keys;
    }

    public function getKeys()
    {
        return $this->keys;
    }

    public function setRow($row)
    {
        $this->row = $row;
    }

    public function getRow()
    {
        return $this->row;
    }



}

class Keypad
{
    protected $buttons;
    const ROWS = 4;
    const COLS = 3;

    const MOVE_VER  = 300;
    const MOVE_HOR  = 200;
    const MOVE_DIAG = 350;
    const PUSH      = 100;
    const WAIT      = 500;

    protected $currentCol = 2;
    protected $currentRow = 4;

    protected $upper;

    protected $invariants;
    const INVARIANTS = ' ,0,1,2,3,4,5,6,7,8,9,9';

    protected $debug;
    const DEBUG_ON  = true;
    const DEBUG_OFF = false;

    public function __construct( $debug = self::DEBUG_OFF )
    {

        $this->invariants = explode(',', self::INVARIANTS);
        $this->buttons    = array();
        $config           = $this->getConfig();
        $this->reset();
        $this->setDebug($debug);

        $col = 1;
        $row = 1;
        foreach ($config as $cfg) {
            $this->buttons[] = new Button(array(
                'col'  => $col,
                'row'  => $row,
                'keys' => (is_string($cfg)) ? explode(',', $cfg) : $cfg,
            ));
            $col ++;
            if ($col > self::COLS) {
                $row ++;
                $col = 1;
                if ($row > self::ROWS) {
                    break;
                }
            }
        }

    }

    /**
     * Positions the keypad at '0' in lower case
     */
    public function reset()
    {
        list ($col, $row, $pos) = $this->posOfKey('0');
        $this->currentCol = $col;
        $this->currentRow = $row;
        $this->upper = false;
    }

    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * Positions and contents of keys in keypad
     *
     * @return array
     */
    public function getConfig()
    {
        return array(
            /* Col 1    Col 2     Col 3  */
                ' ,1', 'a,b,c,2', 'd,e,f,3',   // Row 1
            'c,h,i,4', 'j,k,l,5', 'm,n,o,6',   // Row 2
            'p,q,r,7', 't,u,v,8', 'w,x,y,z,9', // Row 3
            null,      '0',       button::UP   // Row 4

        );
    }

    /**
     * @param $key
     *
     * @return array ($col, $row, $pos)
     */
    public function posOfKey($key)
    {
        foreach($this->buttons as $button)
        {
            $pos = $button->hasKey($key);
            if (false!==$pos) {
                return array($button->getCol(), $button->getRow(), $pos);
            }
        }
        return array(0, 0, 0);
    }

    /**
     * Computes and returns time to go to col and row passed
     *
     * @param $col
     * @param $row
     *
     * @return number
     */
    public function computeTimeToGoTo($col,$row)
    {
        $movesHor  = ($this->currentCol - $col);
        $movesVer  = ($this->currentRow - $row);
        $movesDiag = 0;
        if (abs($movesHor) > 0  && abs($movesVer) > 0) {
            $min = (abs($movesHor)>abs($movesVer)) ? abs($movesVer) : abs($movesHor);
            $movesHor  -= $min;
            $movesVer  -= $min;
            $movesDiag += $min;
        }

        $time = abs($movesHor)  * self::MOVE_HOR +
                abs($movesVer)  * self::MOVE_VER +
                abs($movesDiag) * self::MOVE_DIAG;

        $this->debug(sprintf('GoTo: %d,%d => %d', $col, $row, $time) . PHP_EOL);

        // new position of finger/cursor
        $this->currentCol = $col;
        $this->currentRow = $row;

        return  $time;
    }

    /**
     * Goes to upper key, computes time to go and to Press and returns this time
     *
     * @return number
     */
    public function pressUp()
    {
        $time = $this->computeTimeToGoTo(self::COLS, self::ROWS) + self::PUSH;
        $this->upper = !$this->upper;
        $this->debug(sprintf('Press %s => %d', 'Upper', self::PUSH) . PHP_EOL);

        return $time;
    }

    /**
     * Computes and returns time to press $key, including all times: toGo, toPress, toWait and Upper if necessary
     *
     * @param      $key
     * @param bool $debug
     *
     * @return int|number
     */
    public function computeTimeToPress($key)
    {
        $lower = strtolower($key);
        $isUp  = ($lower != $key);
        if (!in_array($key, $this->invariants) && ($isUp != $this->upper)) {
            $timeToUp = $this->pressUp();
        } else {
            $timeToUp = 0;
        }
        list($col, $row, $pos) = $this->posOfKey($lower);
        $timeToGo    = $this->computeTimeToGoTo($col, $row);
        $timeToPress = (1 + $pos) * self::PUSH;
        $timeToWait  = ($timeToGo == 0) ? self::WAIT : 0;
        if ($timeToWait) {
            $this->debug(sprintf('Wait time => %d', $timeToWait) . PHP_EOL);
        }
        $time = $timeToUp + $timeToGo + $timeToPress + $timeToWait;
        $this->debug(sprintf('Press %s => %d', $key, $timeToPress) . PHP_EOL);

        return $time;
    }

    /**
     * Calculates time to process the string indicated
     *
     * @param $seq string
     *
     * @return int|number
     */
    public function computeTotalTimeToPress($seq)
    {
        $time = 0;
        $arr  = str_split($seq);
        foreach ($arr as $key) {
            $time += $this->computeTimeToPress($key);
            $this->debug(sprintf('Accumulated time %d', $time) . PHP_EOL);
        }

        return $time;
    }

    /**
     * Prints if debug
     *
     * @param $str
     */
    public function debug($str)
    {
        if ($this->getDebug()) {
            print $str;
        }
    }

}

$keypad = new Keypad(Keypad::DEBUG_ON);
$f      = fopen('php://stdin', 'r');
while ($value = fgets($f))
{
    $keypad->reset();
    print $value . ' ' . $keypad->computeTotalTimeToPress($value) . PHP_EOL;
}
fclose($f);
